# README #

### USAGE

<div id="myft-instafeed"><div>

var instafeed = new Instafeed({
	target: 'myft-instafeed',
	user: 'lancaperfume',
	limit: 3
});

instafeed.run();

### CUSTOMIZE

See the Instafeed.prototype.options Object to know what you can do.

### DEPENDENCIES

 - A jQuery.ajax plugin as well as the jQuery itself to make cross domain requests.
 - Art.js script to abstract dom creation.