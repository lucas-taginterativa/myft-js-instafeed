/**
 * Instafeed
 * ---
 * @author Lucas Teles
 * @version 0.10
 *
 */

 (function($, art) {

 	var Instafeed;

 	Instafeed = (function() {

 		var _self;

 		function Instafeed(params) {

 			_self = this;

 			var option, value;

 			if (typeof params === 'object') {
 				for (option in params) {
 					value = params[option];
 					_self.options[option] = value;
 				}
 			}
 		};

 		Instafeed.prototype.options = {

 			target: 'myft-instafeed',

			user: 'taginterativa',
			resolution: 'thumbnail',
			baseUrl: 'http://www.instagram.com/',
			resUrl: '/media',

			limit: 10,

			ui: {

				container: {

					elem: "ul",
					attr: {

						class: "container"
					}
				},

				item: {

					elem: "li",
					attr: { class: "item" },
					child: {

						elem: "img",
						attr: function(item) { return { src: item.images.thumbnail.url}; }
					}
				}
			}
 		};

 		var _createInstance = function(item) {

 			var elem = art( _self.options.ui.item.elem, _self.options.ui.item.attr,
				art( _self.options.ui.item.child.elem, _self.options.ui.item.child.attr(item))
			);

			return elem;
 		};

 		var _sendToView = function(data) {

 			var _items 	   = _extractData(data),
 				_target 	   = document.getElementById(_self.options.target);

 			var container = art(_self.options.ui.container.elem);

 			for (var i = 0, length = _items.length; i < length; i++) {

 				art(container, _createInstance(_items[i]));
 			}

 			art(_target, container);
 		};

 		var _extractData = function(data) {

 			var _json  = data.replace(/(<([^>]+)>)/ig,""), // remove html tags (<html>,<head>,<body>,...)
 				_obj   = JSON.parse(_json);

 			var _limit = _self.options.limit;

 			if ( ! _obj.items) throw new Error('Empty items!');

 			if (_limit) _obj.items = _obj.items.slice(0, _limit);

 			return _obj.items;
 		};

 		Instafeed.prototype.run = function() {

 			var _url = _self.options.baseUrl + _self.options.user + _self.options.resUrl;

 			$.ajax({

				url: _url,
				type: 'GET',
				success: function(res) {
					_sendToView(res.responseText);
				}
			});
 		};

 		return Instafeed;

 	})();

 	(function(root, factory) {
 		if (typeof define === 'function' && define.amd) {
 			return define([], factory);
 		} else if (typeof module === 'object' && module.exports) {
 			return module.exports = factory();
 		} else {
 			return root.Instafeed = factory();
 		}
 	})(this, function() {
 		return Instafeed;
 	});

 }).call(this, $, art);